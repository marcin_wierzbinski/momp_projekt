
	
		<footer id="grve-footer" class="grve-section">

		<?php grve_print_footer_widgets(); ?>
			<section class="grve-footer-bar">
				<div class="grve-container">
					<div class="grve-row">

						<div class="grve-column-1-2">
							<div class="grve-copyright" style="line-height:68px;">Copyright 2014 - All Rights Reserved.</div>
						</div>
						<div class="grve-column-1-2 text-right">
							<img src="<?php print IMG ?>/logo_adream.png" />
						</div>

					</div>
				</div>
			</section>

		</footer>
	</div>
		
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRngKslUGJTlibkQ3FkfTxj3Xss1UlZDA&sensor=false"></script>
		<script src="<?php print JS ?>/maps.js"></script>
		<?php wp_footer(); ?>
	</body>

</html>