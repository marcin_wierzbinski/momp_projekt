<?php
/* 
Rows
Columns
Buttons
Dividers


*/


/*************Rows **************/

if (!function_exists('row')) {
function row($atts, $content = null) {
    return '<div class="row">' . do_shortcode($content) . '</div>';
}
}
add_shortcode('row', 'row');

/*************Columns **************/

if (!function_exists('column_12')) {
function column_12($atts, $content = null) {
    return '<div class="large-12 small-12 medium-12 columns">' . do_shortcode($content) . '</div>';
}
}
add_shortcode('column_12', 'column_12');

if (!function_exists('column_4')) {
function column_4($atts, $content = null) {
extract(shortcode_atts(array( "font_size" => ""), $atts));
    return '<div class="large-4 medium-4 columns">' . do_shortcode($content) . '</div>';
}
}
add_shortcode('column_4', 'column_4');

if (!function_exists('column_6')) {
function column_6($atts, $content = null) {
extract(shortcode_atts(array( "class" => ""), $atts));
    return '<div class="large-6 small-6 medium-6 columns '. $class.'">' . do_shortcode($content) . '</div>';
}
}
add_shortcode('column_6', 'column_6');

/*************Panel Element **************/

if (!function_exists('panel')) {
function panel($atts, $content = null) {
    return '<div class="panel">' . do_shortcode($content) . '</div>';
}
}
add_shortcode('panel', 'panel');


if (!function_exists('button_blue')) {
function button_blue($atts, $content = null) {
    return '<button class="blue">' . do_shortcode($content) . '</button>';
}
}
add_shortcode('button_blue', 'button_blue');

if (!function_exists('button_red')) {
function button_red($atts, $content = null) {
    return '<button class="red">' . do_shortcode($content) . '</button>';
}
}
add_shortcode('button_red', 'button_red');

/* DIVIDERS */

if (!function_exists('divider_40')) {
function divider_40() {
    return '<div class="divider_40"></div>';
}
}
add_shortcode('divider_40', 'divider_40');

if (!function_exists('divider_30')) {
function divider_30() {
    return '<div class="divider_30"></div>';
}
}
add_shortcode('divider_30', 'divider_30');

if (!function_exists('divider_20')) {
function divider_20() {
    return '<div class="divider_20"></div>';
}
}
add_shortcode('divider_20', 'divider_20');

if (!function_exists('divider_10')) {
function divider_10() {
    return '<div class="divider_10"></div>';
}
}
add_shortcode('divider_10', 'divider_10');

/* Dividers */ 

if (!function_exists('min_content')) {
function min_content($atts, $content = null) {
extract(shortcode_atts(array('img' => '', 'title' => ''), $atts));
    return '<div class="minContentRow">
			<div class="minContentColumn25">
				<img src="'. $img .'" alt="icon"/>
			</div>
			<div class="minContentColumn75">
				<h3><b>'. $title.'</b></h3>
				<p>' . do_shortcode($content) . '</p>
			</div>
			</div>';
	}
	}
add_shortcode('min_content', 'min_content');

if (!function_exists('ElementBackground')) {
function ElementBackground($atts, $content = null) {
extract(shortcode_atts(array('title' => ''), $atts));
    return '<div class="ElementBackground">
				<div class="row collapse text-center">
					<div class="ElementOverlapSection">
						<div class="ElementOverlap">'. $title .'</div>
                    </div>' . do_shortcode($content) . '
				</div>
			</div>';
}
}
add_shortcode('ElementBackground', 'ElementBackground');

if (!function_exists('ElementBlok')) {
function ElementBlok($atts, $content = null) {
extract(shortcode_atts(array('title' => ''), $atts));
    return '<div class="blok">
             <div class="header"><h1>'.$title .'</h1> </div>
				<p> 
					' . do_shortcode($content) . '
				</p>  
				</div>';
}
}
add_shortcode('ElementBlok', 'ElementBlok');


