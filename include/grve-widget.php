<?php 
function grve_register_sidebars() {

	register_sidebar( array(
		'id' => 'grve-default-sidebar',
		'name' => __( 'Main Sidebar', GRVE_THEME_TRANSLATE ),
		'description' => __( 'Main Sidebar Widget Area', GRVE_THEME_TRANSLATE ),
		'before_widget' => '<div id="%1$s" class="grve-widget widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h5 class="grve-widget-title">',
		'after_title' => '</h5>',
	));
	register_sidebar( array(
		'id' => 'grve-sidebar-1',
		'name' => __( 'Sidebar 1', GRVE_THEME_TRANSLATE ),
		'description' => __( 'Sidebar 1 Widget Area', GRVE_THEME_TRANSLATE ),
		'before_widget' => '<div id="%1$s" class="grve-widget widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h5 class="grve-widget-title">',
		'after_title' => '</h5>',
	));
	register_sidebar( array(
		'id' => 'grve-sidebar-2',
		'name' => __( 'Sidebar 2', GRVE_THEME_TRANSLATE ),
		'description' => __( 'Sidebar 2 Widget Area', GRVE_THEME_TRANSLATE ),
		'before_widget' => '<div id="%1$s" class="grve-widget widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h5 class="grve-widget-title">',
		'after_title' => '</h5>',
	));
	register_sidebar( array(
		'id' => 'grve-sidebar-3',
		'name' => __( 'Sidebar 3', GRVE_THEME_TRANSLATE ),
		'description' => __( 'Sidebar 3 Widget Area', GRVE_THEME_TRANSLATE ),
		'before_widget' => '<div id="%1$s" class="grve-widget widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h5 class="grve-widget-title">',
		'after_title' => '</h5>',
	));
	register_sidebar( array(
		'id' => 'grve-sidebar-4',
		'name' => __( 'Sidebar 4', GRVE_THEME_TRANSLATE ),
		'description' => __( 'Sidebar 4 Widget Area', GRVE_THEME_TRANSLATE ),
		'before_widget' => '<div id="%1$s" class="grve-widget widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h5 class="grve-widget-title">',
		'after_title' => '</h5>',
	));

	if ( grve_woocommerce_enabled() ) {

		register_sidebar( array(
			'id' => 'grve-woocommerce-sidebar-shop',
			'name' => __( 'Shop Overview Page', GRVE_THEME_TRANSLATE ),
			'description' => __( 'Shop Overview Widget Area', GRVE_THEME_TRANSLATE ),
			'before_widget' => '<div id="%1$s" class="grve-widget widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h5 class="grve-widget-title">',
			'after_title' => '</h5>',
		));
		register_sidebar( array(
			'id' => 'grve-woocommerce-sidebar-product',
			'name' => __( 'Shop Product Pages', GRVE_THEME_TRANSLATE ),
			'description' => __( 'Shop Product Widget Area', GRVE_THEME_TRANSLATE ),
			'before_widget' => '<div id="%1$s" class="grve-widget widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h5 class="grve-widget-title">',
			'after_title' => '</h5>',
		));
	}

	register_sidebar( array(
		'id' => 'grve-footer-1-sidebar',
		'name' => __( 'Footer 1', GRVE_THEME_TRANSLATE ),
		'description' => __( 'Footer 1 Widget Area', GRVE_THEME_TRANSLATE ),
		'before_widget' => '<div id="%1$s" class="grve-widget widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h2 class="grve-widget-title">',
		'after_title' => '</h2>',
	));
	register_sidebar( array(
		'id' => 'grve-footer-2-sidebar',
		'name' => __( 'Footer 2', GRVE_THEME_TRANSLATE ),
		'description' => __( 'Footer 2 Widget Area', GRVE_THEME_TRANSLATE ),
		'before_widget' => '<div id="%1$s" class="grve-widget widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h2 class="grve-widget-title">',
		'after_title' => '</h2>',
	));
	register_sidebar( array(
		'id' => 'grve-footer-3-sidebar',
		'name' => __( 'Footer 3', GRVE_THEME_TRANSLATE ),
		'description' => __( 'Footer 3 Widget Area', GRVE_THEME_TRANSLATE ),
		'before_widget' => '<div id="%1$s" class="grve-widget widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h2 class="grve-widget-title">',
		'after_title' => '</h2>',
	));
	register_sidebar( array(
		'id' => 'grve-footer-4-sidebar',
		'name' => __( 'Footer 4', GRVE_THEME_TRANSLATE ),
		'description' => __( 'Footer 4 Widget Area', GRVE_THEME_TRANSLATE ),
		'before_widget' => '<div id="%1$s" class="grve-widget widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h2 class="grve-widget-title">',
		'after_title' => '</h2>',
	));
}
