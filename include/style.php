<?php 
	function hosting_scripts() {
	/* Global Style*/
	wp_enqueue_style( 'foundation', get_template_directory_uri() . '/css/foundation.css' );
	wp_enqueue_style( 'fontAwesome', get_template_directory_uri() . '/css/font-awesome.min.css', array('foundation') );
	wp_enqueue_style( 'plugins', get_template_directory_uri() . '/css/plugins.css', array('fontAwesome') );
	wp_enqueue_style( 'basic', get_template_directory_uri() . '/css/basic.css', array('plugins') );
	wp_enqueue_style( 'themeStyle', get_template_directory_uri() . '/css/theme-style.css', array('basic') );
	wp_enqueue_style( 'grid', get_template_directory_uri() . '/css/grid.css', array('themeStyle') );
	wp_enqueue_style( 'elements', get_template_directory_uri() . '/css/elements.css', array('grid') );
	
	wp_enqueue_script('modernizr',get_stylesheet_directory_uri() . '/js/modernizr.custom.js"',array( 'jquery' ), '1.0.0', true);
	wp_enqueue_script('bootstrap',get_stylesheet_directory_uri() . '/js/bootstrap.js',array( 'jquery' ), '1.0.0', true);
	/* End Global Style */
	
	
}
add_action( 'wp_enqueue_scripts', 'hosting_scripts' );