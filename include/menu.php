<?php 

function register_menu() {
  register_nav_menus(
    array(
		'header-menu' => __( 'Menu Top' ),
		'header-menu-top' => __( 'Menu Top Bar' ),
    )
  );
}
add_action( 'init', 'register_menu' );