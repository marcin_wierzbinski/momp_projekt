<?php 
	function hosting_scripts() {
	/* Global Style*/
	wp_enqueue_style( 'foundation', get_template_directory_uri() . '/css/foundation.css' );
	wp_enqueue_style( 'fontAwesome', get_template_directory_uri() . '/css/font-awesome.min.css', array('foundation') );
	wp_enqueue_style( 'basic', get_template_directory_uri() . '/css/basic.css', array('fontAwesome') );
	wp_enqueue_style( 'themeStyle', get_template_directory_uri() . '/css/theme-style.css', array('basic') );
	wp_enqueue_style( 'grid', get_template_directory_uri() . '/css/grid.css', array('themeStyle') );
	wp_enqueue_style( 'elements', get_template_directory_uri() . '/css/elements.css', array('grid') );
	
	wp_enqueue_script('modernizr',get_stylesheet_directory_uri() . '/js/modernizr.custom.js"',array( 'jquery' ), '1.0.0', true);
	//wp_enqueue_script('appear',get_stylesheet_directory_uri() . '/js/main.js"',array( 'modernizr' ), '1.0.0', true);
	
	//if(is_page_template('kontakt.php')) {
	//	wp_enqueue_script( 'mapsapiscript','https://maps.googleapis.com/maps/api/js?key=AIzaSyCRngKslUGJTlibkQ3FkfTxj3Xss1UlZDA&sensor=false',array( 'mapsapiscript' ), '1.0.0', true);
	//	wp_enqueue_script('googleMaps',get_stylesheet_directory_uri() . '/js/maps.js"',array( 'mapsapiscript' ), '1.0.0', true);
	//}
	
	
}
add_action( 'wp_enqueue_scripts', 'hosting_scripts' );