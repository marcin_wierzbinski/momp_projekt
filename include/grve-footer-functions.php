<?php

/*
*	Footer Helper functions
*
* 	@version	1.0
* 	@author		Greatives Team
* 	@URI		http://greatives.eu
*/

/**
 * Prints Footer Widgets
 */
function grve_print_footer_widgets() {



		$grve_footer_widgets = array(
			'grve-footer-1-sidebar' => is_active_sidebar( 'grve-footer-1-sidebar' ),
			'grve-footer-2-sidebar' => is_active_sidebar( 'grve-footer-2-sidebar' ),
			'grve-footer-3-sidebar' => is_active_sidebar( 'grve-footer-3-sidebar' ),
			'grve-footer-4-sidebar' => is_active_sidebar( 'grve-footer-4-sidebar' ),
		);

		$grve_column_calc = 0;
		$grve_footer_tablet_class = '';

		foreach ( $grve_footer_widgets as $key => $value ) {
			if ( $value ) {
				$grve_column_calc++;
			}
		}
		if ( 4 == $grve_column_calc ) {
			$grve_footer_tablet_class = 'grve-tablet-column-1-2';
		}

		if ( 0 != $grve_column_calc ) {
			echo '<div class="grve-container">';
			echo '  <div class="grve-row">';
			foreach ( $grve_footer_widgets as $key => $value ) {
				if ( $value ) {
					echo '<div class="grve-column-1-' . $grve_column_calc . ' ' . $grve_footer_tablet_class . '">';
					dynamic_sidebar( $key );
					echo '</div>';
				}
			}
			echo '  </div>';
			echo '</div>';
		}
}

/**
 * Prints Footer Copyright Area
 */
function grve_print_footer_copyright() {

	if ( grve_visibility( 'footer_copyright_visibility' ) ) {
		if ( is_singular() && 'yes' == grve_post_meta( 'grve_disable_copyright' ) ) {
			return;
		}
		if( grve_woocommerce_enabled() ) {
			// Disabled Footer Copyright in Shop
			if ( is_shop() && !is_search() && 'yes' == grve_post_meta_shop( 'grve_disable_copyright' ) ) {
				return false;
			}
		}
?>
	<section class="grve-footer-bar">
		<div class="grve-container">
			<div class="grve-row">

				<div class="grve-column-1-2">
					<div class="grve-copyright">
						<?php echo do_shortcode( grve_option( 'footer_copyright' ) ); ?>
					</div>
				</div>
				<div class="grve-column-1-2">
					<div class="grve-second-menu">
						<?php grve_footer_nav(); ?>
					</div>
				</div>

			</div>
		</div>
	</section>

<?php
	}
}

?>