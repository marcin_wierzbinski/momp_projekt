<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
	<link href="<?php bloginfo('stylesheet_url') ?>" rel="stylesheet"/>
	<?php wp_head(); ?>
	<style>
		.qrve-top-menu{
		background:#376d97;
		height:50px;
		}
		#grve-theme-body{
		top:0px !important;
		}
		#grve-header{
		position:static !important;
		}
		nav.top-bar{
		display: block !important;
		}
		.reset-m{
		margin:0px;
		padding:0px;
		}
	</style>
</head>

<body id="grve-body" <?php body_class(); ?>>
		<div id="grve-wrapper">
			<div class="qrve-top-menu">
				<nav class="top-bar reset-m">
					<div class="grve-container">
						<div class="grve-row reset-m" style="float:right;"> 
						 <?php
							$defaults = array(
								'theme_location'  => 'header-menu-top',
								'echo'            => true,
								'fallback_cb'     => 'wp_page_menu',
								'items_wrap'      => '<ul  "class="menu" id="qrve-menu-top">%3$s</ul>',
								'depth'           => 0,
							);
							wp_nav_menu( $defaults );
							?>
						</div>
					</div>
				</nav>
			</div>
		</div>
			
					<div id="grve-theme-body">
                            <header class="grve-style-2 grve-header-light" data-height="90" id="grve-header">
                                <div class="grve-section grve-container container">
										<h1 class="grve-logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img alt="Logo Miejski Ośrodek " src="<?php print IMG ?>/logos/logo.png" title="testy"></a></h1>
                                    <ul class="grve-header-options">
                                        <li>
                                            <a class="grve-menu-btn" href="#"><img alt="Main Menu" src=
                                            "<?php print IMG ?>/graphics/menu-button.svg"></a>
                                        </li>
                                    </ul>
                                    <nav class="grve-horizontal-menu">
                                         <?php
											$defaults = array(
												'theme_location'  => 'header-menu',
												'echo'            => true,
												'fallback_cb'     => 'wp_page_menu',
												'items_wrap'      => '<ul style="line-height:1.2;" class="grve-menu" id="menu-test">%3$s</ul>',
												'depth'           => 0,
											);
											wp_nav_menu( $defaults );
											?>
                                    </nav>
                                </div>
                            </header>