<?php 

define('THEMEROOT', get_stylesheet_directory_uri());
define('IMG', THEMEROOT . '/images');
define('JS', THEMEROOT . '/js');
/* clear <p></p> */
function my_formatter($content) {
        $new_content = '';
        $pattern_full = '{(\[raw\].*?\[/raw\])}is';
        $pattern_contents = '{\[raw\](.*?)\[/raw\]}is';
        $pieces = preg_split($pattern_full, $content, -1, PREG_SPLIT_DELIM_CAPTURE);

        foreach ($pieces as $piece) {
                if (preg_match($pattern_contents, $piece, $matches)) {
                        $new_content .= $matches[1];
                } else {
                        $new_content .= wptexturize(wpautop($piece));
                }
        }

        return $new_content;
}

remove_filter('the_content', 'wpautop');
remove_filter('the_content', 'wptexturize');

add_filter('the_content', 'my_formatter', 99);

add_action( 'admin_menu', 'remove_menu_pages' );
function remove_menu_pages() {
	remove_menu_page('edit-comments.php');
}



require_once( get_template_directory() . '/include/grve-footer-functions.php' );
//require_once( get_template_directory() . '/include/grve-widget.php' );
include_once('include/styleScript.php');
include_once('include/menu.php');
include_once('include/shortcodes.php');
//include_once('include/custom-header.php');


function grve_register_sidebars() {

	register_sidebar( array(
		'id' => 'grve-default-sidebar',
		'name' => __( 'Main Sidebar'),
		'description' => __( 'Main Sidebar Widget Area' ),
		'before_widget' => '<div id="%1$s" class="grve-widget widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h5 class="grve-widget-title">',
		'after_title' => '</h5>',
	));
	register_sidebar( array(
		'id' => 'grve-sidebar-1',
		'name' => __( 'Sidebar 1'),
		'description' => __( 'Sidebar 1 Widget Area'),
		'before_widget' => '<div id="%1$s" class="grve-widget widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h5 class="grve-widget-title">',
		'after_title' => '</h5>',
	));
	register_sidebar( array(
		'id' => 'grve-sidebar-2',
		'name' => __( 'Sidebar 2'),
		'description' => __( 'Sidebar 2 Widget Area'),
		'before_widget' => '<div id="%1$s" class="grve-widget widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h5 class="grve-widget-title">',
		'after_title' => '</h5>',
	));
	register_sidebar( array(
		'id' => 'grve-sidebar-3',
		'name' => __( 'Sidebar 3'),
		'description' => __( 'Sidebar 3 Widget Area'),
		'before_widget' => '<div id="%1$s" class="grve-widget widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h5 class="grve-widget-title">',
		'after_title' => '</h5>',
	));
	register_sidebar( array(
		'id' => 'grve-sidebar-4',
		'name' => __( 'Sidebar 4'),
		'description' => __( 'Sidebar 4 Widget Area'),
		'before_widget' => '<div id="%1$s" class="grve-widget widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="grve-widget-title">',
		'after_title' => '</h3>',
	));

	register_sidebar( array(
		'id' => 'grve-footer-1-sidebar',
		'name' => __( 'Footer 1'),
		'description' => __( 'Footer 1 Widget Area'),
		'before_widget' => '<div id="%1$s" class="grve-widget widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="grve-widget-title">',
		'after_title' => '</h3>',
	));
	register_sidebar( array(
		'id' => 'grve-footer-2-sidebar',
		'name' => __( 'Footer 2'),
		'description' => __( 'Footer 2 Widget Area'),
		'before_widget' => '<div id="%1$s" class="grve-widget widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="grve-widget-title">',
		'after_title' => '</h3>',
	));
	register_sidebar( array(
		'id' => 'grve-footer-3-sidebar',
		'name' => __( 'Footer 3'),
		'description' => __( 'Footer 3 Widget Area'),
		'before_widget' => '<div id="%1$s" class="grve-widget widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="grve-widget-title">',
		'after_title' => '</h3>',
	));
	register_sidebar( array(
		'id' => 'grve-footer-4-sidebar',
		'name' => __( 'Footer 4'),
		'description' => __( 'Footer 4 Widget Area'),
		'before_widget' => '<div id="%1$s" class="grve-widget widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="grve-widget-title">',
		'after_title' => '</h3>',
	));
}
add_action( 'widgets_init', 'grve_register_sidebars' );